package com.ltmonitor.mongo;

import java.io.Serializable;

import com.mongodb.ReflectionDBObject;

public class GpsModel extends ReflectionDBObject implements Serializable {
	private static final long serialVersionUID = 1L;
	
	// 车终端卡号
	private String simNo;

	// 车牌号
	private String plateNo;

	// 司机标识
	private String driverId;

	// 司机姓名
	private String driverName;

	// 设备终端状态
	private String status;

	// 报警状态
	private String alarmState;

	// 经度
	private double longitude;

	// 纬度
	private double latitude;

	// 海拔
	private double altitude;

	// 速度
	private double velocity;
	
	//对经纬度的地理位置解析
	private String location;
	
	//方向
	private int direction;
	
	//里程
	private double mileage;
	
	//油量
	private double gas;
	
	//行驶记录仪速度
	private double recordVelocity;
	
	private boolean valid;
	
	private String runStatus;

	// 发送时间
	private java.util.Date sendTime = new java.util.Date(0);

	// 入库时间
	private java.util.Date createTime = new java.util.Date(0);


	public String getSimNo() {
		return simNo;
	}

	public String getPlateNo() {
		return plateNo;
	}

	public String getDriverId() {
		return driverId;
	}

	public String getDriverName() {
		return driverName;
	}

	public String getStatus() {
		return status;
	}

	public String getAlarmState() {
		return alarmState;
	}

	public double getLongitude() {
		return longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public double getAltitude() {
		return altitude;
	}

	public double getVelocity() {
		return velocity;
	}

	public java.util.Date getSendTime() {
		return sendTime;
	}



	public void setSimNo(String simNo) {
		this.simNo = simNo;
	}

	public void setPlateNo(String plateNo) {
		this.plateNo = plateNo;
	}

	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setAlarmState(String alarmState) {
		this.alarmState = alarmState;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public void setAltitude(double altitude) {
		this.altitude = altitude;
	}

	public void setVelocity(double velocity) {
		this.velocity = velocity;
	}

	public void setSendTime(java.util.Date sendTime) {
		this.sendTime = sendTime;
	}

	public String getLocation() {
		return location;
	}

	public int getDirection() {
		return direction;
	}

	public double getMileage() {
		return mileage;
	}

	public double getGas() {
		return gas;
	}

	public double getRecordVelocity() {
		return recordVelocity;
	}

	public boolean isValid() {
		return valid;
	}

	public String getRunStatus() {
		return runStatus;
	}

	public java.util.Date getCreateTime() {
		return createTime;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public void setDirection(int direction) {
		this.direction = direction;
	}

	public void setMileage(double mileage) {
		this.mileage = mileage;
	}

	public void setGas(double gas) {
		this.gas = gas;
	}

	public void setRecordVelocity(double recordVelocity) {
		this.recordVelocity = recordVelocity;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public void setRunStatus(String runStatus) {
		this.runStatus = runStatus;
	}

	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
	


}