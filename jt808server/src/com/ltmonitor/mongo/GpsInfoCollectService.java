package com.ltmonitor.mongo;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.ltmonitor.entity.GpsInfo;
import com.mongodb.DB;
import com.mongodb.DBCollection;

public class GpsInfoCollectService {

	DB db = MongoTemplate.getConnect("erp");

	public void insertGpsInfo(List<GpsInfo> gpsInfoList) {
		if (gpsInfoList != null && gpsInfoList.size() > 0) {
			for (GpsInfo gpsInfo : gpsInfoList) {
				DBCollection collection = db.getCollection("gps");
				String dataId = UUID.randomUUID().toString();
				gpsInfo.set_id(dataId);
				collection.insert(gpsInfo);
			}
		}
	}


	
	public static void main(String[] args) {
		List<GpsInfo> gpsModelList = new ArrayList<GpsInfo>();
		GpsInfo m = new GpsInfo();
		gpsModelList.add(m);
		new GpsInfoCollectService().insertGpsInfo(gpsModelList);
	}
}
