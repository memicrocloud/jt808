package com.ltmonitor.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import com.ltmonitor.dao.IBaseDao;
import com.ltmonitor.entity.Department;
import com.ltmonitor.entity.GPSRealData;
import com.ltmonitor.entity.Terminal;
import com.ltmonitor.entity.VehicleData;
import com.ltmonitor.service.IDepartmentService;
import com.ltmonitor.service.IVehicleService;

public class VehicleService implements IVehicleService {

	private IBaseDao baseDao;

	private IDepartmentService departmentService;	
	/**
	 * 车辆基本信息缓存
	 */
	public static HashMap<Integer, VehicleData> vehicleMap = new HashMap<Integer,VehicleData>();

	public static ConcurrentMap<String, GPSRealData> realDataMap = new ConcurrentHashMap<String, GPSRealData>();

	public VehicleData getVehicleData(int vehicleId) {
		if(vehicleMap.containsKey(vehicleId))
		{
			return vehicleMap.get(vehicleId);
		}
		VehicleData vd =  (VehicleData) baseDao.load(VehicleData.class, vehicleId);
		vehicleMap.put(vehicleId, vd);
		return vd;
	}

	// 获取车辆信息数据
	public final VehicleData getVehicleByPlateNo(String plateNo) {
		String hql = "from VehicleData v where v.plateNo =  ? and deleted = ?";

		VehicleData v = (VehicleData) getBaseDao().find(hql,
				new Object[] { plateNo, false });
		return v;
	}

	// 获取车辆信息数据
	public final VehicleData getVehicleBySimNo(String simNo) {
		String hql = "from VehicleData v where v.simNo =  ? and deleted = ?";

		VehicleData v = (VehicleData) getBaseDao().find(hql,
				new Object[] { simNo, false });
		return v;
	}

	public final Terminal getTerminal(int terminalId) {
		return (Terminal) baseDao.load(Terminal.class, terminalId);
	}

	/**
	 * 终端唯一ID
	 * 
	 * @param terminalNo
	 * @return
	 */
	public Terminal getTerminalByTermNo(String terminalNo) {
		String hql = "from Terminal where termNo = ? and deleted = ?";
		return (Terminal) baseDao.find(hql, new Object[] { terminalNo, false });
	}

	/**
	 * 查询车辆的车辆部门
	 */
	public Department getDepartmentByPlateNo(String plateNo) {
		VehicleData vd = getVehicleData(plateNo);
		if (vd != null && vd.getDepId() > 0) {
			return getDepartmentService().getDepartment(vd.getDepId());
		}
		return null;
	}

	public VehicleData getVehicleData(String plateNo) {
		String hsql = "from VehicleData where plateNo = ?";

		VehicleData vd = (VehicleData) baseDao.find(hsql, plateNo);
		return vd;
	}
	/**
	 * 保存终端
	 * @param t
	 * @throws Exception
	 */
	public void saveTerminal(Terminal t) throws Exception
	{
		String hsql = "from Terminal where termNo = ? and entityId <> ? and deleted = ? ";
		Terminal otherVd = (Terminal) baseDao.find(hsql, new Object[] {
				t.getTermNo() , t.getEntityId(), false });
		if (otherVd != null) {			
			String msg = "终端ID重复,无法保存!";
			throw new Exception(msg);
		}
		this.baseDao.saveOrUpdate(t);
	}
	/**
	 * 保存车辆信息
	 */
	public void saveVehicleData(VehicleData vd) throws Exception {
		String hsql = "from VehicleData where (plateNo= ? or simNo = ?) and entityId <> ? and deleted = ? ";
		VehicleData otherVd = (VehicleData) baseDao.find(hsql, new Object[] {
				vd.getPlateNo(), vd.getSimNo(), vd.getEntityId(), false });
		if (otherVd != null) {
			String msg = otherVd.getPlateNo().equals(vd.getPlateNo()) ? "车牌号重复,"
					: "";
			msg += otherVd.getSimNo().equals(vd.getSimNo()) ? "卡号重复," : "";
			msg += "无法保存!";
			throw new Exception(msg);
		}
		// 更新实时表的数据
		if (vd.getEntityId() > 0) {
			hsql = "from GPSRealData where vehicleId = ?";
			GPSRealData rd = (GPSRealData) baseDao.find(hsql, vd.getEntityId());
			if (rd != null) {
				rd.setSimNo(vd.getSimNo());
				rd.setPlateNo(vd.getPlateNo());
				this.baseDao.saveOrUpdate(rd);
			}
		}
		this.baseDao.saveOrUpdate(vd);
		this.vehicleMap.put(vd.getEntityId(), vd);
	}

	/**
	 * 根据部门id列表，查询出部门所辖车辆列表
	 */
	@Override
	public List<VehicleData> getVehicleListByDepId(List<Integer> depIdList) {
		String hql = "from VehicleData where depId in (:depIdList) and deleted = 0 order by plateNo";
		if (depIdList.size() == 0) {
			depIdList.add(0);
		}
		List ls = getBaseDao().queryByNamedParam(hql, "depIdList",
				depIdList.toArray());
		List<VehicleData> result = new ArrayList<VehicleData>();
		for (Object o : ls) {
			result.add((VehicleData) o);
		}
		return result;
	}

	public IBaseDao getBaseDao() {
		return baseDao;
	}

	public void setBaseDao(IBaseDao baseDao) {
		this.baseDao = baseDao;
	}

	public IDepartmentService getDepartmentService() {
		return departmentService;
	}

	public void setDepartmentService(IDepartmentService departmentService) {
		this.departmentService = departmentService;
	}

}
