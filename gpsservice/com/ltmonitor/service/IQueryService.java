package com.ltmonitor.service;

import java.util.List;

import com.ltmonitor.entity.VehicleData;

public interface IQueryService {

	public abstract List<VehicleData> getVehicleListByUserId(int userId);

}